import logo from './logo.svg';
import './App.css';
import BinanceCom from './Components/com_binance/com_binance';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useEffect, useState } from 'react';
import Login from './Components/login/login';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(() => {
    // Kiểm tra nếu có thông tin đăng nhập trong localStorage thì tự động đăng nhập
    const loggedIn = localStorage.getItem('isLoggedIn');
    if (loggedIn === 'true') {
      setIsLoggedIn(true);
    }
  }, []);
  return (
    <div className="App">
      {!isLoggedIn ? (
        <Login onLogin={setIsLoggedIn} />
      ) : (
        <>
        <BinanceCom />
        <ToastContainer />
        </>
      )}
    </div>
    
  );
}

export default App;
