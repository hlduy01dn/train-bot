import React, { Component } from "react";
import axios from "axios";
import styled from "styled-components";
import "./style.css";
import { toast } from "react-toastify";
import { confirmAlert } from "react-confirm-alert"; // Import
import "react-confirm-alert/src/react-confirm-alert.css"; // Import css
// const BASE_URL = process.env.SERVER_URL
const BASE_URL = "https://train-bot-server.onrender.com";
// const BASE_URL = "http://localhost:8000";

const Container = styled.div`
  font-family: Arial, sans-serif;
  font-size: 0.8em;
  max-width: 700px; // Adjust this value to your liking
  margin: 0 auto; // This will center the container
`;

const Title = styled.h2`
  color: #333;
  text-align: center;
`;

const ScrollableDiv = styled.div`
  max-height: 240px;
  overflow-y: auto;
`;

const Table = styled.table`
  width: 100%;
  border-collapse: collapse;
  margin-top: 20px;
`;

const TableRow = styled.tr`
  border-bottom: 1px solid #ddd;
  background-color: ${(props) =>
    props.colorCondition ? "rgba(242, 242, 242)" : "white"};
`;
const TableCell = styled.td`
  color: ${(props) => (props.colorCondition ? "green" : "red")};
`;

const StatusCell = styled(TableCell)`
  color: ${(props) => (props.status === "open" ? "green" : "black")};
`;

class BinanceCom extends Component {
  constructor(props) {
    super(props);
    this.fetchOpenOrders = this.fetchOpenOrders.bind(this);

    this.state = {
      amount: 0,
      parts: 0,
      price: 0,
      increase: 5,
      decrease: 5,
      discount: 0,
      currency: "BNB",
      orders: [],
      arrSymbol: [],
      openOrders: [],
      currentAccount: "Select",
      buyOption: "buybyamount",
      showNotification: false,
    };

  }
  handleAccountChange = (event) => {
    console.log("Changing account to", event.target.value);
    const selectedAccount = event.target.value;
    let selectedKey = "";

    if (selectedAccount === "1") {
      selectedKey =
        "";
    } else if (selectedAccount === "2") {
      selectedKey =
        "";
    }

    selectedKey = selectedKey.slice(-10);

    this.setState({ currentAccount: selectedAccount, currentKey: selectedKey }, () => {
      this.setCurrentAccount();
    });

    localStorage.setItem("currentAccount", selectedAccount);
    localStorage.setItem("currentKey", selectedKey);
  };
  setCurrentAccount = () => {
    const account = this.state.currentAccount || "1";
    console.log("Setting account to", account);

    if (!account) {
      console.error("Account is null or undefined");
      return;
    }

    axios
      .post(`${BASE_URL}/setCurrentAccount`, { account })
      .then((res) => {
        console.log(account);
        if (res.data.status === "success") {
          console.log(res.data.message);
          toast.success(res.data.message, { autoClose: 5000 }); // Display the success message to the user for 5 seconds
          window.location.reload();
        } else {
          console.error(res.data.message);
          toast.error(res.data.message, { autoClose: 5000 }); // Display the error message to the user for 5 seconds
        }
      })
      .catch((err) => {
        console.error(err);
        toast.error("An error occurred: " + err.message, { autoClose: 5000 }); // Display the error message to the user for 5 seconds
      });
  };
  handlebuyOptionChange(event) {
    this.setState({ buyOption: event.target.value });
  }
  addNewCurrency = () => {
    this.setState({ currency: this.state.newCurrency });
  };

  handleIncreaseChange = (e) => {
    this.setState({ increase: e.target.value });

    // Show the notification
    this.setState({ showNotification: true });

    // Hide the notification after 3 seconds
    setTimeout(() => {
      this.setState({ showNotification: false });
    }, 2000);
  };

  async componentDidMount() {
    const currentAccount = localStorage.getItem("currentAccount");
    const currentKey = localStorage.getItem("currentKey");
  
    if (currentAccount || currentKey) {
      this.setState({ currentAccount, currentKey });
      
    }
  
    this.updateBalance();
    this.fetchOpenOrders();
    this.fetchPrices();
    this.updateSymbolList();
  
    this.priceInterval = setInterval(this.fetchPrices, 10000);
    this.orderInterval = setInterval(this.fetchOpenOrders, 5000);
    this.historyInterval = setInterval(this.fetchOrderHistory, 1000);
  }
  
  componentWillUnmount() {
    clearInterval(this.interval);
    clearInterval(this.priceInterval); // Clear price fetch interval
    clearInterval(this.historyInterval);

  }
  
fetchOrderHistory = async () => {
  try {
    const res = await axios.get(`${BASE_URL}/historyAllOrders`);
    const oldClosedOrders = this.state.orders.filter(
      (order) => order.status === "closed"
    );

    const sanitizedOrders = res.data.map(order => ({
      orderId: order.orderId || '',
      amount: order.amount || 0,
      price: order.price || '',
      tokenCoin: order.tokenCoin || '',
      status: order.status || 'unknown',
      side: order.side || 'unknown'
    }));

    this.setState({ orders: sanitizedOrders });
    const newClosedOrders = sanitizedOrders.filter(
      (order) => order.status === "closed"
    );
    if (newClosedOrders.length > oldClosedOrders.length) {
      this.updateBalance();
    }
  } catch (error) {
    console.error("Failed to fetch order history:", error);
    if (error.message.includes("InsufficientFunds")) {
      alert("Insufficient balance for requested action");
    }
  }
}
  handleLogout = () => {
    confirmAlert({
      title: "Confirm to logout",
      message: "Are you sure you want to logout?",
      buttons: [
        {
          label: "Yes",
          onClick: () => {
            localStorage.removeItem("isLoggedIn");
            window.location.reload();
          },
        },
        {
          label: "No",
          onClick: () => {},
        },
      ],
    });
  };

  render() {
    const defaultSymbols = ["BNB", "BTC", "ETH"];
    const additionalSymbols = this.state.arrSymbol.filter(
      (symbol) => !defaultSymbols.includes(symbol.symb)
    );

    const sortedArrSymbol = this.state.arrSymbol.sort((a, b) => {
      if (a.symb === "USDT") return 1;
      if (b.symb === "USDT") return -1;
      return 0;
    });
    return (
      <>
        <div className="logout-container">
          <button onClick={this.handleLogout} className="logout-btn">
            <i className="fas fa-sign-out-alt"></i> <strong>Logout</strong>
          </button>
        </div>

        <div className="BinanceCom container">
          <h3 className="my-3">TRADING BOT</h3>
          <div>
            <label>
              Select Account:
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "flex-start",
                  flexDirection: "column",
                }}
              >
                <select
                  onChange={this.handleAccountChange}
                  value={this.state.currentAccount}
                  style={{
                    width: "150px",
                    margin: "0px 10px",
                    padding: "10px",
                    borderRadius: "5px",
                    border: "1px solid #ccc",
                  }}
                >
                  <option value="Select" disabled>
                    Select Account
                  </option>
                  <optgroup label="Binance">
                    <option
                      value="1"
                      className={
                        this.state.currentAccount === "1"
                          ? "option-current-account"
                          : ""
                      }
                    >
                      Account 1
                    </option>
                    <option
                      value="2"
                      className={
                        this.state.currentAccount === "2"
                          ? "option-current-account"
                          : ""
                      }
                    >
                      Account 2
                    </option>
                  </optgroup>
                  <optgroup label="OKX">
                    <option
                      value="okx"
                      className={
                        this.state.currentAccount === "okx"
                          ? "option-current-account"
                          : ""
                      }
                    >
                      OKX
                    </option>
                  </optgroup>
                  <optgroup label="MEXC">
                    <option
                      value="mxc"
                      className={
                        this.state.currentAccount === "mxc"
                          ? "option-current-account"
                          : ""
                      }
                    >
                      MEXC
                    </option>{" "}
                  </optgroup>
                </select>
                {this.state.currency ? (
                  <div style={{ display: "flex", alignItems: "center" }}>
                    {this.state.currentKey && <p>...</p>}
                    <p
                      style={{
                        color: "dodgerblue",
                        fontWeight: "bold",
                      }}
                    >
                      {this.state.currentKey}
                    </p>
                  </div>
                ) : null}
              </div>
            </label>
          </div>

          <div style={{ display: "flex", alignItems: "center" }}>
            <input
              type="text"
              className="form-control"
              value={this.state.newSymbol}
              onChange={(e) =>
                this.setState({ newSymbol: e.target.value.toUpperCase() })
              }
              style={{
                width: "150px",
                margin: "0px 10px",
                padding: "10px",
                borderRadius: "5px",
                border: "1px solid #ccc",
              }}
              placeholder="Add new symbol"
            />
            <button
              onClick={this.updateBalance}
              style={{
                padding: "7px 10px",
                borderRadius: "5px",
                border: "none",
                backgroundColor: "#4CAF50",
                color: "white",
                cursor: "pointer",
              }}
            >
              Fetch Balance
            </button>
          </div>

          <div style={{ maxHeight: "400px", overflowY: "auto" }}>
            <table
              className="table table-striped"
              style={{ marginTop: "20px" }}
            >
              <thead>
                <tr>
                  <th>Symbols</th>
                  <th>Amount</th>
                  <th>Price (USDT)</th>
                </tr>
              </thead>
              <tbody>
                {sortedArrSymbol.map((sym, i) => (
                  <tr key={i}>
                    <td>{sym.symb}</td>
                    <td>
                      {isNaN(parseFloat(sym.free))
                        ? "0"
                        : parseFloat(sym.free).toLocaleString("en-US", {
                            maximumFractionDigits: 3,
                          })}
                    </td>
                    <td>
                      {sym.price
                        ? parseFloat(sym.price) !== 0
                          ? parseFloat(sym.price).toLocaleString("en-US", {
                              maximumFractionDigits: 10,
                            })
                          : sym.price.replace(/^0+\B|(?<!\.)0+$/g, "")
                        : "USDT"}
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>

          <div className="my-3 d-flex align-items-center justify-content-between"></div>
          <div className="d-flex align-items-center"></div>
          <div className="my-3 d-flex align-items-center justify-content-between">
            <div className="d-flex align-items-center">
              <h3
                style={{
                  margin: "0 10px",
                  fontSize: "18px",
                  fontWeight: "bold",
                }}
              >
                Buy/Sell{" "}
              </h3>
              <input
                type="text"
                className="form-control custom-input"
                value={this.state.amount}
                onChange={(txt) => this.setState({ amount: txt.target.value })}
                style={{
                  width: "150px",
                  margin: "0 10px",
                  padding: "10px",
                  borderRadius: "5px",
                  border: "1px solid #ccc",
                }}
              />
              <select
                className="form-control custom-select"
                value={this.state.currency}
                onChange={(e) => this.setState({ currency: e.target.value })}
                style={{
                  width: "150px",
                  margin: "0 10px",
                  padding: "10px",
                  borderRadius: "5px",
                  border: "1px solid #ccc",
                }}
              >
                {/* Luôn hiển thị các đồng tiền mặc định */}
                {defaultSymbols.map((symbol) => (
                  <option key={symbol} value={symbol}>
                    {symbol}
                  </option>
                ))}
                {/* Hiển thị các đồng tiền sở hữu khác, không trùng lặp */}
                {additionalSymbols.map((symbol) => (
                  <option key={symbol.symb} value={symbol.symb}>
                    {symbol.symb}
                  </option>
                ))}
              </select>
              {/* <label htmlFor="price-input">Limit Price:</label> */}
              <input
                id="price-input"
                type="text"
                className="form-control custom-input"
                value={this.state.price}
                onChange={(txt) => this.setState({ price: txt.target.value })}
                style={{
                  width: "150px",
                  margin: "0 10px",
                  padding: "10px",
                  borderRadius: "5px",
                  border: "1px solid #ccc",
                }}
              />
              <h3
                style={{
                  margin: "0 10px",
                  fontSize: "18px",
                  fontWeight: "bold",
                }}
              >
                {" "}
                {/* USDT */}
              </h3>
            </div>
            <div className="">
              <button
                className="btn btn-primary"
                onClick={() => {
                  this.sendBuyOrder();
                }}
                style={{
                  margin: "0 10px",
                  padding: "10px 20px",
                  borderRadius: "5px",
                  fontSize: "16px",
                  backgroundColor: "#007bff",
                  borderColor: "#007bff",
                }}
              >
                Buy {this.state.amount} {this.state.currency}
              </button>
              <button
                className="btn btn-primary"
                onClick={() => {
                  this.sendSellOrder();
                }}
                style={{
                  margin: "0 10px",
                  padding: "10px 20px",
                  borderRadius: "5px",
                  fontSize: "16px",
                  backgroundColor: "#dc3545",
                  borderColor: "#007bff",
                }}
              >
                Sell {this.state.amount} {this.state.currency}
              </button>
            </div>
          </div>

          <div style={{ display: "flex", justifyContent: "space-between" }}>
            {/* Set Sell Orders */}
            <div style={{ width: "45%" }}>
              <h3 className="my-3">Set Sell Orders</h3>

              <div className="form-group">
                <label style={{ display: "block", textAlign: "left" }}>
                  Amount:
                </label>

                <input
                  type="number"
                  className="form-control"
                  value={this.state.amount}
                  onChange={(e) => this.setState({ amount: e.target.value })}
                  style={{
                    color: "#000",
                    border: "1px solid  #cccccc",
                    padding: "10px",
                    borderRadius: "5px",
                  }}
                  disabled={this.state.hasOpenOrder}
                />

                <label style={{ display: "block", textAlign: "left" }}>
                  Parts:{" "}
                </label>
                <input
                  type="number"
                  className="form-control"
                  value={this.state.parts}
                  onChange={(e) => this.setState({ parts: e.target.value })}
                  style={{
                    color: "#000",
                    border: "1px solid  #cccccc",
                    padding: "10px",
                    borderRadius: "5px",
                  }}
                  disabled={this.state.hasOpenOrder}
                />

                <label style={{ display: "block", textAlign: "left" }}>
                  Initial Price:
                  <input
                    type="number"
                    className="form-control"
                    value={this.state.price}
                    onChange={(e) => this.setState({ price: e.target.value })}
                    style={{
                      color: "#000",
                      border: "1px solid  #cccccc",
                      padding: "10px",
                      borderRadius: "5px",
                    }}
                    disabled={this.state.hasOpenOrder}
                  />
                </label>
                <label style={{ display: "block", textAlign: "left" }}>
                  Price Increase (%):{" "}
                </label>
                <input
                  type="number"
                  className="form-control"
                  value={this.state.increase}
                  onChange={this.handleIncreaseChange}
                  style={{
                    color: "#000",
                    border: "1px solid  #cccccc",
                    padding: "10px",
                    borderRadius: "5px",
                  }}
                  disabled={this.state.hasOpenOrder}
                />

                <div
                  style={{ display: "flex", justifyContent: "space-between" }}
                >
                  <div style={{ flex: "0 0 65%" }}>
                    <label style={{ display: "block", textAlign: "left" }}>
                      Price Buy again (%):
                    </label>
                    <input
                      type="number"
                      className="form-control"
                      value={this.state.discount}
                      onChange={(e) =>
                        this.setState({ discount: e.target.value })
                      }
                      style={{
                        color: "#000",
                        border: "1px solid  #cccccc",
                        padding: "10px",
                        borderRadius: "5px",
                        width: "100%",
                      }}
                      disabled={this.state.hasOpenOrder}
                    />
                  </div>

                  <div style={{ flex: "0 0 30%" }}>
                    <label style={{ display: "block", textAlign: "left" }}>
                      Buy Option:
                    </label>
                    <select
                      name="buyOption"
                      onChange={this.handlebuyOptionChange.bind(this)}
                      style={{
                        color: "#000",
                        border: "1px solid  #cccccc",
                        padding: "10px",
                        borderRadius: "5px",
                        width: "100%",
                      }}
                    >
                      <option value="">Select an option</option>
                      <option value="buybyamount">Amount</option>
                      <option value="buybyprice">Price</option>
                    </select>
                    {this.state.showNotification && (
                      <p
                        style={{
                          color: "#000",
                          marginTop: "10px",
                          animation: "fadeIn 1s",
                          whiteSpace: "nowrap",
                        }}
                      >
                        The default is to buy <strong>By Amount</strong> .
                      </p>
                    )}
                  </div>
                </div>

                <button
                  disabled={this.state.hasOpenOrder}
                  className="btn btn-primary mt-3"
                  style={{ backgroundColor: "#007bff", borderColor: "#007bff" }}
                  onClick={(e) => {
                    const validation = this.validateForm();
                    if (!validation.isValid) {
                      alert(validation.message);
                    } else {
                      confirmAlert({
                        title: "Confirm to request",
                        message: "Are you sure you want to set sell orders?",
                        buttons: [
                          {
                            label: "Yes",
                            onClick: () => this.setSellOrders.bind(this)(),
                          },
                          {
                            label: "No",
                            onClick: () => {},
                          },
                        ],
                      });
                    }
                  }}
                >
                  Set Sell Orders
                </button>
              </div>
            </div>

            {/* Set Buy Orders */}
            <div style={{ width: "45%" }} className="d-none">
              <h3 className="my-3">Set Buy Orders</h3>
              <div className="form-group">
                <label style={{ display: "block", textAlign: "left" }}>
                  Amount:
                  <input
                    type="number"
                    className="form-control"
                    value={this.state.amount}
                    onChange={(e) => this.setState({ amount: e.target.value })}
                    style={{
                      color: "#000",
                      border: "1px solid  #cccccc",
                      padding: "10px",
                      borderRadius: "5px",
                    }}
                    disabled={this.state.hasOpenOrder}
                  />
                </label>
                <label style={{ display: "block", textAlign: "left" }}>
                  Parts:
                  <input
                    type="number"
                    className="form-control"
                    value={this.state.parts}
                    onChange={(e) => this.setState({ parts: e.target.value })}
                    style={{
                      color: "#000",
                      border: "1px solid  #cccccc",
                      padding: "10px",
                      borderRadius: "5px",
                    }}
                    disabled={this.state.hasOpenOrder}
                  />
                </label>
                <label style={{ display: "block", textAlign: "left" }}>
                  Initial Price:
                  <input
                    type="number"
                    className="form-control"
                    value={this.state.price}
                    onChange={(e) => this.setState({ price: e.target.value })}
                    style={{
                      color: "#000",
                      border: "1px solid  #cccccc",
                      padding: "10px",
                      borderRadius: "5px",
                    }}
                    disabled={this.state.hasOpenOrder}
                  />
                </label>
                <label style={{ display: "block", textAlign: "left" }}>
                  Price Decrease (%):
                  <input
                    type="number"
                    className="form-control"
                    value={this.state.decrease}
                    onChange={(e) =>
                      this.setState({ decrease: e.target.value })
                    }
                    style={{
                      color: "#000",
                      border: "1px solid  #cccccc",
                      padding: "10px",
                      borderRadius: "5px",
                    }}
                    disabled={this.state.hasOpenOrder}
                  />
                </label>

                <button
                  disabled={this.state.hasOpenOrder}
                  className="btn btn-primary mt-3"
                  style={{ backgroundColor: "#007bff", borderColor: "#007bff" }}
                  onClick={(e) => {
                    const validation = this.validateBuyForm();
                    if (!validation.isValid) {
                      alert(validation.message);
                    } else if (
                      window.confirm("Are you sure you want to set buy orders?")
                    ) {
                      this.setBuyOrders.bind(this)();
                    }
                  }}
                >
                  Set Buy Orders
                </button>
              </div>
            </div>
          </div>

          <Container>
            <Title>Transaction History</Title>
            {this.state.orders.length > 0 ? (
              <ScrollableDiv>
                <Table>
                  <thead>
                    <TableRow>
                      <th>ID</th>
                      <th>Amount</th>
                      <th>Price</th>
                      <th>Token</th>
                      <th>Status</th>
                    </TableRow>
                  </thead>
                  <tbody>
                    {this.state.orders.slice().map((order, index) => (
                    <TableRow key={index} colorCondition={index % 2 === 0}>
                    <TableCell>{order.orderId}</TableCell>
                    <TableCell colorCondition={order.side === "Buy"}>{order.amount}</TableCell>
                    <TableCell colorCondition={order.side === "Buy"}>
                      {order.price ? parseFloat(order.price).toFixed(3) : order.price}
                    </TableCell>
                    <TableCell colorCondition={order.side === "Buy"}>{order.tokenCoin}</TableCell>
                    <StatusCell status={order.status || ''}>
                      {order.status ? order.status.charAt(0).toUpperCase() + order.status.slice(1) : ''}
                    </StatusCell>
                  </TableRow>
                  
                    ))}
                  </tbody>
                </Table>
              </ScrollableDiv>
            ) : (
              <p style={{ textAlign: "center", color: "#999" }}>
                No transactions found.
              </p>
            )}
          </Container>

          <div>
            {this.state.openOrders.length > 0 && (
              <>
                <h3 className="my-3"> Total Open Orders</h3>
                <p className="small" style={{ marginBottom: "0" }}></p>
                <table className="table table-striped table-sm">
                  <thead>
                    <tr>
                      <th>Order ID</th>
                      <th>Amount</th>
                      <th>Token</th>
                      <th>Status</th>
                      <th>Price</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.openOrders.map((order) => (
                      <tr key={order.orderId}>
                        <td>{order.orderId}</td>
                        <td>{order.amount}</td>
                        <td>{order.tokenCoin}</td>
                        <td>
                          <span
                            style={{
                              color: order.side === "Sell" ? "red" : "green",
                            }}
                          >
                            {order.side}
                          </span>
                        </td>
                        <td>{order.price}</td>
                        <td>
                          <button
                            className="btn btn-danger btn-sm"
                            onClick={() => this.cancelOrder(order.orderId)}
                          >
                            Cancel
                          </button>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </>
            )}
          </div>

          <br />

          <div></div>
        </div>
      </>
    );
  }

  updateBalance = () => {
    const { newSymbol } = this.state;

    axios
      .post(`${BASE_URL}/balance`, { newSymbol })
      .then((res) => {
        console.log(res.data);
        this.setState({
          arrSymbol: res.data.symbols,
        });

        // Save newSymbol to localStorage
        window.localStorage.setItem("newSymbol", JSON.stringify(newSymbol));
        console.log(JSON.parse(window.localStorage.getItem("newSymbol")));
      })
      .catch((err) => console.log(err));
  };

  sendBuyOrder = () => {
    if (this.state.amount === 0) {
      alert("Amount cannot be 0");
      return;
    }

    axios({
      method: "POST",
      url: `${BASE_URL}/userSendBuyOrder`,
      data: new URLSearchParams({
        amount: this.state.amount,
        symbol: this.state.currency + "/USDT",
        price: this.state.price,
      }),
    })
      .then((res) => {
        if (res.data.error) {
          alert("Error: " + res.data.error);
        } else {
          alert("Order placed successfully!");
          this.updateBalance();
        }
      })
      .catch((error) => {
        console.error("Error:", error);
        alert("An error occurred while processing your request.");
      });
  };

  sendSellOrder = () => {
    if (this.state.amount === 0) {
      alert("Amount cannot be 0");
      return;
    }

    axios({
      method: "POST",
      url: `${BASE_URL}/userSendSellOrder`,
      data: new URLSearchParams({
        amount: this.state.amount,
        symbol: this.state.currency + "/USDT",
        price: this.state.price,
      }),
    })
      .then((res) => {
        if (res.data.error) {
          alert("Error: " + res.data.error);
        } else {
          alert("Sell order placed successfully!");
          this.updateBalance();
        }
      })
      .catch((error) => {
        console.error("Error:", error);
        alert("An error occurred while processing your request.");
      });
  };

  setSellOrders() {
    axios({
      method: "POST",
      url: `${BASE_URL}/setSellOrders`,
      data: new URLSearchParams({
        amount: this.state.amount,
        parts: this.state.parts,
        price: this.state.price,
        increase: this.state.increase,
        discount: this.state.discount,
        currencyPair: this.state.currency + "/USDT",
        buyOption: this.state.buyOption,
      }),
    })
      .then((res) => {
        console.log(res.data);
        this.updateBalance();
        // Display a success message
        toast.success(res.data);
        // window.location.reload();
      })
      .catch((error) => {
        console.log(error);
        if (error.response && error.response.data) {
          // Display the error message from the server using a toast
          toast.error(error.response.data);
        } else {
          toast.error('An error occurred while placing the sell order');
        }
      });
  }
  setBuyOrders() {
    axios({
      method: "POST",
      url: `${BASE_URL}/setBuyOrders`,
      data: new URLSearchParams({
        amount: this.state.amount,
        parts: this.state.parts,
        price: this.state.price,
        decrease: this.state.decrease,
        currencyPair: this.state.currency + "/USDT",
      }),
    }).then((res) => {
      console.log(res.data);
      this.updateBalance();
      window.location.reload();
    });
  }

  cancelOrder(orderId) {
    axios
      .post(`${BASE_URL}/cancelOrder/${orderId}`)
      .then((res) => {
        console.log(res.data);
        this.updateBalance();
        window.location.reload();
      })
      .catch((error) => {
        console.error(error);
      });
  }

  fetchPrices = () => {
    const symbols = this.state.arrSymbol.map((item) => item.symb);
  
    if (this.state.newSymbol) {
      symbols.push(this.state.newSymbol);
    }
  
    symbols.forEach((symbol) => {
      const ws = new WebSocket(`wss://stream.binance.com:9443/ws/${symbol.toLowerCase()}usdt@ticker`);
  
      ws.onmessage = (event) => {
        const response = JSON.parse(event.data);
        this.setState((prevState) => {
          const index = prevState.arrSymbol.findIndex((item) => item.symb === symbol);
          if (index === -1) {
            return {
              arrSymbol: [
                ...prevState.arrSymbol,
                { symb: symbol, price: response.c },
              ],
            };
          } else {
            return {
              arrSymbol: prevState.arrSymbol.map((item, i) =>
                i === index ? { ...item, price: response.c } : item
              ),
            };
          }
        });
      };
  
      ws.onerror = (error) => {
        console.error(`Failed to fetch price for ${symbol}:`, error);
      };
    });
  };
  

  fetchOpenOrders = () => {
    axios
      .get(`${BASE_URL}/historyOrders`)
      .then((res) => {
        const openOrders = res.data.filter((order) => order.status === "open");
        this.setState({ openOrders, hasOpenOrder: openOrders.length > 0 });
      })
      .catch((err) => console.log(err));
  };

  validateForm() {
    let isValid = true;
    let message = "";

    if (this.state.amount <= 0) {
      isValid = false;
      message = "Amount must be greater than 0";
    } else if (this.state.parts <= 0) {
      isValid = false;
      message = "Parts must be greater than 0";
    } else if (this.state.price <= 0) {
      isValid = false;
      message = "Initial Price must be greater than 0";
    } else if (this.state.increase <= 0) {
      isValid = false;
      message = "Price Increase must be greater than 0";
    }

    return { isValid, message };
  }
  updateSymbolList = () => {
    axios
      .post(`${BASE_URL}/balance`, { newSymbol: this.state.newSymbol })
      .then((res) => {
        if (res.data.result) {
          this.setState({ arrSymbol: res.data.symbols });
        }
      })
      .catch((err) => {
        console.error("Failed to fetch symbols:", err.message);
      });
  };

  validateBuyForm() {
    let isValid = true;
    let message = "";

    if (this.state.amount <= 0) {
      isValid = false;
      message = "Amount must be greater than 0";
    } else if (this.state.parts <= 0) {
      isValid = false;
      message = "Parts must be greater than 0";
    } else if (this.state.price <= 0) {
      isValid = false;
      message = "Initial Price must be greater than 0";
    } else if (this.state.decrease <= 0) {
      isValid = false;
      message = "Price Decrease must be greater than 0";
    }

    return { isValid, message };
  }
}

export default BinanceCom;
